﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VIIS.Domain.Core
{
    public class Word
    {
        public Guid Id { get; set; }
        public string InEnglish { get; set; }
        public string ToUkrainian { get; set; }
        public string Client { get; set; }
        public byte[] Pronounsiation { get; set; }

    }
}
