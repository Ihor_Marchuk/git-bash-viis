﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VIIS.Web.Services
{
    public class EmailService
    {

        public async Task SendEmailAsync(string email,string subj,string message)
        {
            var emailMessage =new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("VIIS Administration", "fkca.o15.miv@gmail.com"));
            emailMessage.To.Add(new MailboxAddress(email));
            emailMessage.Subject = subj;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html) { Text = message };

            using (var client=new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 587, false);
                await client.AuthenticateAsync("sample@gmail.com", "YourPassword");
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }

    }
}
