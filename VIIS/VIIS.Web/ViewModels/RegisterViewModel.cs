﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VIIS.Web.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name ="Е_Пошта")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="Пароль не співпадає")]
        [Display(Name = "Підтвердження паролю")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "AGE")]
        public int Age { get; set; }
    }
}
