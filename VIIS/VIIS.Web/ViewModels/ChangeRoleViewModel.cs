﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VIIS.Web.ViewModels
{
    public class ChangeRoleViewModel
    {
        public string UserId { get; set; }
        public string userEmail { get; set; }
        public List<IdentityRole> Roles { get; set; }
        public IList<string> UserRoles { get; set; }
        public ChangeRoleViewModel()
        {
            Roles = new List<IdentityRole>();
            UserRoles = new List<string>();
        }
    }
}
