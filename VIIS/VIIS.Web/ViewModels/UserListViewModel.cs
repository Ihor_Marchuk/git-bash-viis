﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VIIS.Web.Models;

namespace VIIS.Web.ViewModels
{
    public class UserListViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public string Email { get; set; }
    }
}
