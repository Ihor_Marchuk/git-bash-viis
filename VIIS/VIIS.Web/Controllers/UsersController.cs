﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VIIS.Web.Models;
using VIIS.Web.ViewModels;

namespace VIIS.Web.Controllers
{
    public class UsersController : Controller
    {
        UserManager<User> _userManager;
        public UsersController(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public IActionResult AdminOffice() => View(_userManager.Users.ToList());

        public IActionResult CreateNewUser() => View();

        [HttpPost]
        public async Task<IActionResult> CreateNewUser(CreateUserViewMoel model)
        {
            if (ModelState.IsValid)
            {
                User user = new User { Email = model.Email, Age = model.Age, UserName = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("AdminOffice");
                }
                else
                {
                    foreach (var item in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, item.Description);
                    }
                }
            }
            return View(model);
        }

        public async Task<IActionResult> EditUser(string Id)
        {
            User User = await _userManager.FindByIdAsync(Id);
            if (User==null)
            {
                return NotFound();
            }
            EditUserViewModel model = new EditUserViewModel { Id = User.Id, Age = User.Age, Email = User.Email };
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(model.Id);
                if (user!=null)
                {
                    user.Email = model.Email;
                    user.UserName = model.Email;
                    user.Age = model.Age;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("AdminOffice");
                    }
                    else
                    {
                        foreach (var item in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, item.Description);
                        }
                    }
                }
                
            }
             return View(model);
            }


        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user!=null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("AdminOffice");
        }
    }

    }
